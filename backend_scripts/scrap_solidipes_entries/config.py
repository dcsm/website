import os
import yaml


config_path = os.path.join(os.path.dirname(__file__), "config.yaml")


def load_yaml(path: str) -> dict:
    with open(path, "r") as file:
        data = yaml.safe_load(file)
    return data


def load_dcsm_tokens(tokens_yaml_path: str, service_name_prefix: str) -> dict:
    try:
        sessions = load_yaml(tokens_yaml_path)

    except FileNotFoundError:
        return {}

    tokens = {}

    for path, info in sessions.items():
        if service_name_prefix not in path:
            continue

        name = path.split("/")[-1]
        tokens[name] = info["token"]

    return tokens


config = load_yaml(config_path)
common = config["common"]
dcsm = config["dcsm"]
dcsm["tokens"] = load_dcsm_tokens(dcsm["tokens_yaml_path"], dcsm["service_name_prefix"])
renkulab = config["renkulab"]
zenodo = config["zenodo"]
