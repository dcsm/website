from abc import ABC, abstractmethod
import logging
import os

from .config import common as config


class Scrapper(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def get_all_solidipes_entries(self) -> list:
        pass


    @abstractmethod
    def export_entry(self, entry: dict, output_path: str):
        pass


    def export_all_entries(
        self,
        output_dir: str,
        remove_existing: bool = True,
    ):
        if remove_existing:
            self.remove_entries(output_dir)

        entries = self.get_all_solidipes_entries()

        for i, entry in enumerate(entries):
            filename = f"{config['entry_base_name']}-{i + 1:05d}.md"
            output_path = os.path.join(output_dir, filename)
            self.export_entry(entry, output_path)
            logging.info(f"Exported {output_path}")


    def remove_entries(self, dir_path: str):
        """Remove all entry-*.md from directory"""

        files = os.listdir(dir_path)
        entries = [f for f in files if f.startswith(config["entry_base_name"])]

        for entry in entries:
            entry_path = os.path.join(dir_path, entry)
            os.remove(entry_path)
