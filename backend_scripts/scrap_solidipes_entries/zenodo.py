import logging
import os
import requests


from .config import zenodo as config
from .scrapper import Scrapper


class ZenodoScrapper(Scrapper):
    def get_all_solidipes_entries(self) -> list:
        # Search entries
        query = " ".join(config["keywords"])
        entries = get_entries_from_query(query, config["max_entries"])
        entries_by_id = {entry["id"]: entry for entry in entries}

        # Remove unwanted entries
        for entry_id in config["excluded_ids"]:
            entries_by_id.pop(entry_id, None)

        # Add supplementary entries
        for entry_id in config["added_ids"]:
            if entry_id in entries_by_id:
                continue
            entry = get_entry_from_id(entry_id)
            entries_by_id[entry_id] = entry

        entries = list(entries_by_id.values())
        entries.sort(key=lambda entry: entry["created"], reverse=True)

        return entries


    def export_entry(self, entry: dict, output_path: str):
        template_path = os.path.join(os.path.dirname(__file__), config["template_path"])
        with open(template_path, "r") as file:
            template = file.read()

        title = entry["metadata"]["title"].replace('"', r'\"')
        authors = "; ".join([author["name"] for author in entry["metadata"]["creators"]])
        keywords = ", ".join(entry["metadata"].get("keywords", []))

        template = template.replace("{{title}}", title)
        template = template.replace("{{date}}", entry["metadata"]["publication_date"])
        template = template.replace("{{authors}}", authors)
        template = template.replace("{{keywords}}", keywords)
        template = template.replace("{{description}}", "")
        template = template.replace("{{link}}", entry["links"]["self_html"])

        with open(output_path, "w") as file:
            file.write(template)


def get_entries_from_query(query: str, max_entries: int) -> list:
    entries = []

    for page in range(1, max_entries // config["entries_per_page"] + 1):
        new_entries = get_entries_from_query_at_page(query, page)

        if len(new_entries) == 0:
            break

        entries.extend(new_entries)
        logging.info(f"Got {len(entries)} entries from Zenodo")

    entries = entries[:max_entries]
    return entries


def get_entries_from_query_at_page(query: str, page: int = 1) -> list:
    params = {
        "q": query,
        "page": page,
        "size": config["entries_per_page"],
        "sort": config["query_sort_method"],
    }

    response = requests.get(config["url_api"], params=params)
    if response.status_code != 200:
        raise RuntimeError(f"Failed to get entries from Zenodo. Response {response.status_code} - {response.text}")

    data = response.json()
    try:
        entries = data["hits"]["hits"]
    except KeyError:
        raise RuntimeError(f"Failed to get entries from Zenodo. Response: {data}")

    return entries


def get_entry_from_id(entry_id: str) -> dict:
    url = f"{config['url_api']}/{entry_id}"
    response = requests.get(url)
    if response.status_code != 200:
        raise RuntimeError(f"Failed to get entries from Zenodo. Response {response.status_code} - {response.text}")

    entry = response.json()
    return entry


def main():
    import sys

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} output_dir")
        sys.exit(1)

    output_dir = sys.argv[1]
    scrapper = ZenodoScrapper()
    scrapper.export_all_entries(output_dir)
