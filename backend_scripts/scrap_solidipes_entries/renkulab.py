import logging
import os
import re
import requests
import yaml

from .config import renkulab as config
from .scrapper import Scrapper


class RenkuLabScrapper(Scrapper):
    def get_all_solidipes_entries(self) -> list:
        query = " ".join(config["keywords"])
        entries = get_entries_from_query(query) 
        entries = filter_entries_by_name(entries, config["project_name_regex"])
        entries.sort(key=lambda entry: entry["created_at"])
        return entries


    def export_entry(self, entry: dict, output_path: str):
        template_path = os.path.join(os.path.dirname(__file__), config["template_path"])
        with open(template_path, "r") as file:
            template = file.read()

        gitlab_url = entry["web_url"]
        metadata = get_solidipes_metadata(gitlab_url)

        title = metadata.get("title", entry["name"]).replace('"', r'\"')
        date = entry["created_at"].split("T")[0]
        authors = "; ".join([author["name"] for author in metadata.get("creators", [])])
        keywords = ", ".join(metadata.get("keywords", []))
        link = gitlab_url.replace("gitlab.renkulab.io", "renkulab.io/projects")
        session_link = link + "/sessions/new?autostart=1"

        template = template.replace("{{title}}", title)
        template = template.replace("{{date}}", date)
        template = template.replace("{{authors}}", authors)
        template = template.replace("{{keywords}}", keywords)
        template = template.replace("{{description}}", "")
        template = template.replace("{{link}}", link)
        template = template.replace("{{session_link}}", session_link)

        with open(output_path, "w") as file:
            file.write(template)


def get_solidipes_metadata(gitlab_url: str) -> dict:
    metadata_url = gitlab_url + "/-/raw/master/.solidipes/study_metadata.yaml?ref_type=heads&inline=false"
    response = requests.get(metadata_url)

    if response.status_code != 200:
        logging.warning(f"Failed to get metadata from {gitlab_url}. Response {response.status_code} - {response.text}")
        return {}

    try:
        raw_metadata = response.text
        metadata = yaml.safe_load(raw_metadata)
        return metadata

    except Exception as e:
        logging.warning(f"Failed to parse metadata from {gitlab_url}. Error: {e}")
        return {}


def get_entries_from_query(query: str) -> list:
    url = config["url_api"]
    params = {
        "scope": "projects",
        "search": query,
    }
    response = requests.get(url, params=params)

    if response.status_code != 200:
        raise RuntimeError(f"Failed to get entries from RenkuLab. Response {response.status_code} - {response.text}")

    entries = response.json()
    logging.info(f"Got {len(entries)} entries from RenkuLab")
    return entries


def filter_entries_by_name(entries: list, regex: str) -> list:
    return [entry for entry in entries if re.search(regex, entry["name"])]


def main():
    import sys

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} output_dir")
        sys.exit(1)

    output_dir = sys.argv[1]
    scrapper = RenkuLabScrapper()
    scrapper.export_all_entries(output_dir)
