import requests
import logging
import os
from typing import Optional

from .config import dcsm as config, load_yaml
from .scrapper import Scrapper


class DcsmScrapper(Scrapper):
    def get_all_solidipes_entries(self) -> list:
        url = 'https://dcsm.epfl.ch/dcsm-api/curations'
        r = requests.get(url, timeout=1000)
        entries = r.json()
        return entries.items()


    def export_entry(self, entry: dict, output_path: str):
        template_path = os.path.join(os.path.dirname(__file__), config["template_path"])
        with open(template_path, "r") as file:
            template = file.read()

        name, data = entry
        metadata = data['metadata']

        url = data['url']
        title = metadata.get("title", name).replace('"', r'\"')
        date = data['date']
        desc_lines = metadata['description'].split('\n')
        desc_lines = ["    " + l for l in desc_lines]
        description = "\n".join(desc_lines)
        authors = "; ".join([author["name"].replace(',', '') for author in metadata.get("creators", [])])
        keywords = ", ".join(metadata.get("keywords", [])).replace("[", "").replace("]", "")
        link = f"https://dcsm.epfl.ch/dcsm-intranet/?curation={name}"

        template = template.replace("{{name}}", name)        
        template = template.replace("{{title}}", title)
        template = template.replace("{{date}}", date if date is not None else "Not running")
        template = template.replace("{{authors}}", authors)
        template = template.replace("{{keywords}}", keywords)
        template = template.replace("{{description}}", description)
        template = template.replace("{{link}}", link)

        with open(output_path, "w") as file:
            file.write(template)


def get_solidipes_metadata(entry: dict) -> dict:
    local_solidipes_path = get_local_solidipes_path(entry)
    if local_solidipes_path is None:
        return {}

    metadata_path = os.path.join(local_solidipes_path, ".solidipes", "study_metadata.yaml")
    try:
        metadata = load_yaml(metadata_path)
    except FileNotFoundError:
        logging.warning(f"Metadata file not found for {entry['name']}")
        return {}

    return metadata


def get_local_solidipes_path(entry: dict) -> Optional[str]:
    mount_paths = [mount["Source"] for mount in entry["mounts"]]

    for mount_path in mount_paths:
        if config["service_name_prefix"] in mount_path:
            return mount_path

    return None


def main():
    import sys

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} output_dir")
        sys.exit(1)

    output_dir = sys.argv[1]
    scrapper = DcsmScrapper()
    scrapper.export_all_entries(output_dir)
