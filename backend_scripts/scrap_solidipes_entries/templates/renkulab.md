---
title: "{{title}}"
date: {{date}}
params:
  link: "{{link}}"
  authors: "{{authors}}"
  description: "{{description}}"
  keywords: [{{keywords}}]
  session_link: "{{session_link}}"
---

{{authors}}

**Keywords:** {{keywords}}

[See on RenkuLab]({{link}})  
[Launch live session]({{session_link}})
