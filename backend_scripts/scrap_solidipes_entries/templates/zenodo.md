---
title: "{{title}}"
date: {{date}}
params:
  link: "{{link}}"
  authors: "{{authors}}"
  description: "{{description}}"
  keywords: [{{keywords}}]
---

{{authors}}

**Keywords:** {{keywords}}

[See on Zenodo]({{link}})
