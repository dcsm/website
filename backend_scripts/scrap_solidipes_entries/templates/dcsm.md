---
title: "{{name}}: {{title}}"
date: {{date}}
params:
  link: "{{link}}"
  authors: "{{authors}}"
  description: |
{{description}}
  keywords: [{{keywords}}]
---

{{authors}}

**Keywords:** {{keywords}}

[Launch live session]({{link}}) (needs authentication)
