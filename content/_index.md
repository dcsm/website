---
title: "Solidipes@DCSM"

description: "Dissemination of Computational Solid Mechanics"
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: '2.8.2'
cascade:
  featured_image: 'images/solidipes.jpg'
params:
  recent_posts_number: 5
---

DCSM project is deploying to the web the *Open Data Curation tool* **Solidipes**, made by scientists for scientists. 

DCSM is an *Open Research Data* (ORD) project funded by the **ETH-Board**.

Solidipes is in close relation to **Renku**.

Instructions for getting started can be found [on the online Documentation](https://solidipes.readthedocs.io/en/latest/)
