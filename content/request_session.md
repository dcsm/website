---
title: Request session
description: Ask for a Solidipes session hosted on DCSM
menu:
  main:
    weight: 1
---

We can host your project for visualization, curation and sharing purposes. Files are stored on an internal S3 system.

<div style="text-align: center">
<a href="https://dcsm.epfl.ch/dcsm-intranet/?op=creation">
  <button style="background-color:#a4d61e;margin-top:6px;margin-bottom:16px;border-radius:4px;font-size:1.6em;padding:8px 20px;    font-family: "GibsonSemibold", "Helvetica Neue", Helvetica, Arial, sans-serif;float:none !important;text-shadow:0 1px 1px rgba(0,0,0,0.2)">Create a new Curation for a Dataset (requires ORCID login)</button>
</a>
</div>

