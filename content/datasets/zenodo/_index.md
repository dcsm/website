---
title: Zenodo datasets
type: datasets
featured_image: "https://raw.githubusercontent.com/zenodo/zenodo/master/zenodo/modules/theme/static/img/logos/zenodo-gradient-square.svg"
cascade:
  type: datasets-entry
  featured_image: "https://raw.githubusercontent.com/zenodo/zenodo/master/zenodo/modules/theme/static/img/logos/zenodo-gradient-square.svg"
---

List of datasets curated with Solidipes and published on Zenodo.
